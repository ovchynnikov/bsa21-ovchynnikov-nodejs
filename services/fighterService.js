const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    create(body) {
        const createdFighter = FighterRepository.create(body);
        if (!createdFighter) {
            throw Error('Fighter is not created');
        }
        return createdFighter;
    }

    getAll() {
        const items = FighterRepository.getAll();
        if (items.length < 1) {
            throw Error('Fighters not found');
        }
        return items;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item || item.length < 1) {
            throw Error('Fighter not found');
        }
        return item;
    }

    updateFighter(id, dataToUpdate) {
        const updatedFighter = FighterRepository.update(id, dataToUpdate);
        if (!updatedFighter) {
            throw Error('Fighter is not updated');
        }
        return updatedFighter;
    }

    deleteFighter(fighter) {
        const deletedFighter = FighterRepository.delete(fighter);
        if (!deletedFighter) {
            throw Error('Fighter not found');
        }
        return deletedFighter;
    }
}

module.exports = new FighterService();