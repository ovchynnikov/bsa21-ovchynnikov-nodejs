const { user } = require('../models/user');
const { UserRepository } = require('../repositories/userRepository');

const createUserValid = (req, res, next) => {

    // TODO: Implement validatior for user entity during creation
    try {
        if (validateAllRows(req.body) &&
            req.body.firstName.length >= 1 &&
            req.body.lastName.length >= 1 &&
            validateEmail(req.body.email) &&
            validatePhoneNumber(req.body.phoneNumber) &&
            req.body.password.length >= 3) {
            next();
        } else throw new Error(`User entity to create is not valid`);

    } catch (error) {
        res.error = error;
        next();
    }
}

const userObjectKeys = Object.keys(user);

function validateAllRows(incomeObject) {
    const incomeObjectKeys = Object.keys(incomeObject);

    function contains(userObjectKeys, incomeObjectKeys) {
        if (incomeObjectKeys.length >= 1) {
            for (var i = 0; i < incomeObjectKeys.length; i++) {
                if (userObjectKeys.indexOf(incomeObjectKeys[i]) == -1) return false;
            }
            return true;
        } else return false;
    }
    let result = contains(userObjectKeys, incomeObjectKeys);

    return result;
}

function validatePhoneNumber(number) {
    const regEx = /^[+]?\d+$/;

    if (number && regEx.test(number) && number.slice(0, 4) === '+380' && number.length === 13) {
        return true
    } else {
        return false
    }
}

function validateEmail(email) {

    const re = /(\W|^)[\w.+\-]*@gmail\.com(\W|$)/i;
    return re.test(email);
}

const updateUserValid = (req, res, next) => {
    if (res.error) {
        next();
    }
    try {
        if (validateAllRows(req.body) &&
            ((req.body.firstName || req.body.email === 0) ? req.body.firstName.length >= 1 : true) &&
            ((req.body.lastName || req.body.lastName === 0) ? req.body.lastName.length >= 1 : true) &&
            ((req.body.email || req.body.email === 0) ? validateEmail(req.body.email) : true) &&
            ((req.body.phoneNumber || req.body.phoneNumber === 0) ? validatePhoneNumber(req.body.phoneNumber) : true) &&
            ((req.body.password || req.body.password === 0) ? (req.body.password && req.body.password.length >= 3) : true)

        ) {
            next();
        } else throw new Error(`User entity to update is not valid`);

    } catch (error) {
        res.error = error;
        next();
    }
}


const validateBodyMiddlware = (req, res, next) => {

    try {
        if (req.body.hasOwnProperty("id")) {
            throw new Error(`You can't send id in request body`);
        } else next();
    } catch (error) {
        res.error = error;
        next();
    }
}

const userDataExists = (req, res, next) => {
    if (res.error) {
        next();
    }
    try {
        const phone = req.body.phoneNumber;
        const email = req.body.email.toLowerCase();
        const userNumberExists = UserRepository.getOne({ "phoneNumber": phone });
        const userEmailExists = UserRepository.getOne({ "email": email });
        if (userNumberExists) {
            throw new Error(`User with this phone ${req.body.phoneNumber} is already registered`);
        } else if (userEmailExists) {
            throw new Error(`User with email ${req.body.email} is already registered`);
        } else next();
    } catch (error) {
        res.error = error;
        next();
    }
}


exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.userDataExists = userDataExists;
exports.validateBodyMiddlware = validateBodyMiddlware;