const { fighter } = require('../models/fighter');
const { FighterRepository } = require('../repositories/fighterRepository');


const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation

    try {
        if (validateAllRows(req.body) &&
            req.body.name.length >= 1 &&
            validatePower(req.body.power) &&
            validateDefense(req.body.defense) &&
            validateHealth(req, req.body.health)
        ) {
            next();
        } else throw new Error(`Fighter entity to create is not valid`);

    } catch (error) {
        res.error = error;
        next();
    }
}

const regEx = /^[0-9]+$/;

function validateHealth(req, health) {

    if (health && regEx.test(health) && (health >= 80 && health <= 120)) {
        return true
    }
    if (!req.body.hasOwnProperty("health")) {
        req.body.health = 100;
        return true;
    } else false;
}

function validatePower(power) {
    if (power && regEx.test(power) && (power >= 1 && power <= 100)) {
        return true
    } else {
        return false
    }
}

function validateDefense(defense) {

    if (defense && regEx.test(defense) && (defense >= 1 && defense <= 10)) {
        return true
    } else {
        return false
    }
}

const fighterObjectKeys = Object.keys(fighter);

function validateAllRows(incomeObject) {
    const incomeObjectKeys = Object.keys(incomeObject);

    function contains(fighterObjectKeys, incomeObjectKeys) {
        if (incomeObjectKeys.length >= 1) {
            for (var i = 0; i < incomeObjectKeys.length; i++) {
                if (fighterObjectKeys.indexOf(incomeObjectKeys[i]) == -1) return false;
            }
            return true;
        } else return false;
    }
    let result = contains(fighterObjectKeys, incomeObjectKeys);

    return result;
}


const updateFighterValid = (req, res, next) => {
    if (res.error) {
        next();
    }
    try {
        if (validateAllRows(req.body) &&
            (req.body.name ? req.body.name.length >= 1 : true) &&
            ((req.body.power || req.body.power === 0) ? validatePower(req.body.power) : true) &&
            ((req.body.defense || req.body.defense === 0) ? validateDefense(req.body.defense) : true) &&
            ((req.body.health || req.body.health === 0) ? validateHealth(req.body.health) : true)

        ) {
            next();
        } else throw new Error(`Fighter entity to update is not valid`);

    } catch (error) {
        res.error = error;
        next();
    }
}

const validateBodyMiddlware = (req, res, next) => {

    try {
        if (req.body.hasOwnProperty("id")) {
            throw new Error(`You can't send id in request body`);
        } else next();
    } catch (error) {
        res.error = error;
        next();
    }
}

const fighterDataExists = (req, res, next) => {
    if (res.error) {
        next();
    }
    try {
        const name = req.body.name;
        const fighterNameExists = FighterRepository.getOne({ "name": name });
        if (fighterNameExists) {
            throw new Error(`User with this name ${req.body.name} is already registered`);
        } else next();
    } catch (error) {
        res.error = error;
        next();
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
exports.validateBodyMiddlware = validateBodyMiddlware;
exports.fighterDataExists = fighterDataExists;