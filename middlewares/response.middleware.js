const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    if (!res.error) {
        res.status(200).json(res.data ? res.data : 'Success');

    } else if (res.error.message.includes('is not') || res.error.message.includes('is already registered') || res.error.message.includes(`You can't send`)) {
        res.status(400).send({
            error: true,
            message: res.error.message
        })

    } else if (res.error.message.includes('not found')) {

        res.status(404).send({
            error: true,
            message: res.error.message
        })

    } else {
        res.status(400).send({
            error: true,
            message: res.error
        })
    }
};

exports.responseMiddleware = responseMiddleware;