const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid, validateBodyMiddlware, fighterDataExists } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.post('/', createFighterValid, fighterDataExists, validateBodyMiddlware, (req, res, next) => {
    if (res.error) {
        next();
    } else {
        try {
            const fighter = FighterService.create(req.body);
            res.data = fighter;
        } catch (error) {
            res.error = error;
        } finally {
            next();
        }
    }

}, responseMiddleware)

router.get('/', validateBodyMiddlware, (req, res, next) => {

    try {
        const fighters = FighterService.getAll();
        res.data = fighters;
    } catch (error) {
        res.error = error;
    } finally {
        next();
    }

}, responseMiddleware)

router.get('/:id', validateBodyMiddlware, (req, res, next) => {
    if (res.error) {
        next();
    }
    try {
        const fighter = FighterService.search(req.params);
        res.data = fighter;
    } catch (error) {
        res.error = error;
    } finally {
        next();
    }

}, responseMiddleware)

router.put('/:id', validateBodyMiddlware, updateFighterValid, (req, res, next) => {
    if (res.error) {
        next();
    } else {
        try {
            const fighter = FighterService.updateFighter(req.params.id, req.body);
            res.data = fighter;
        } catch (error) {
            res.error = error;
        } finally {
            next();
        }
    }

}, responseMiddleware);


router.delete('/:id', validateBodyMiddlware, (req, res, next) => {
    try {
        if (FighterService.search(req.params)) {

            const deletedFighter = FighterService.deleteFighter(req.params.id);
            if (deletedFighter.length > 0) {
                res.data = deletedFighter;
            }
        }
    } catch (error) {
        res.error = error;
    } finally {
        next();
    }

}, responseMiddleware);
module.exports = router;